# Flectra Community / timesheet

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[hr_timesheet_sheet_attendance](hr_timesheet_sheet_attendance/) | 1.0.0.0.0| HR Timesheet Sheet Attendance
[project_task_stage_allow_timesheet](project_task_stage_allow_timesheet/) | 1.0.1.0.0|         Allows to tell that a task stage is opened for timesheets.
[hr_timesheet_sheet](hr_timesheet_sheet/) | 1.0.2.0.0| Timesheet Sheets, Activities
[hr_timesheet_task_required](hr_timesheet_task_required/) | 1.0.1.0.1|         Set task on timesheet as a mandatory field
[crm_timesheet](crm_timesheet/) | 1.0.1.0.0| CRM Timesheet


